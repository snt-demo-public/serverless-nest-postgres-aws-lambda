import * as Joi from 'joi'
import { getMetadataArgsStorage } from 'typeorm'

export interface EnvConfig {
  [key: string]: string
}

export class ConfigService {
  private readonly envConfig: EnvConfig

  constructor() {
    const config = process.env

    config.NODE_ENV = process.env.NODE_ENV || 'local'

    this.envConfig = this.validateInput(config)
  }

  /**
   * Ensures all needed variables are set, and returns the validated JavaScript object
   * including the applied default values.
   */
  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      NODE_ENV: Joi.string()
        .valid('development', 'local')
        .default('development'),
      TYPEORM_CONNECTION: Joi.string().required(),
      TYPEORM_HOST: Joi.string().required(),
      TYPEORM_USERNAME: Joi.string().optional(),
      TYPEORM_PASSWORD: Joi.string().optional(),
      TYPEORM_DATABASE: Joi.string().required(),
      TYPEORM_PORT: Joi.number().required(),
      TYPEORM_SYNCHRONIZE: Joi.string().valid('true', 'false'),
      TYPEORM_LOGGING: Joi.string().required(),
      TYPEORM_ENTITIES: Joi.string().optional(),
      TYPEORM_MIGRATIONS: Joi.string().optional(),
      TYPEORM_MIGRATIONS_RUN: Joi.string().valid('true', 'false'),
      TYPEORM_MIGRATIONS_DIR: Joi.string().optional(),
    })
    .unknown(true)

    const {
      error,
      value: validatedEnvConfig,
    } = envVarsSchema.validate(envConfig)

    if (error) {
      throw new Error(`Config validation error: ${error.message}`)
    }

    return validatedEnvConfig
  }

  get databaseConfig(): object {
    const dbConfig = {
      type: String(this.envConfig.TYPEORM_CONNECTION),
      host: String(this.envConfig.TYPEORM_HOST),
      port: Number(this.envConfig.TYPEORM_PORT),
      username: String(this.envConfig.TYPEORM_USERNAME || ''),
      password: String(this.envConfig.TYPEORM_PASSWORD || ''),
      database: String(this.envConfig.TYPEORM_DATABASE),
      entities: getMetadataArgsStorage().tables.map((tbl) => tbl.target),
      synchronize: String(this.envConfig.TYPEORM_SYNCHRONIZE) === 'true',
      logging: String(this.envConfig.TYPEORM_LOGGING) === 'true',
      connectTimeout: 10000,
      keepConnectionAlive: true,
    }

    return Object(dbConfig)
  }
}
