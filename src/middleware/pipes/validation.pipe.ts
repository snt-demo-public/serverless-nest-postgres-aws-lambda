import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException } from '@nestjs/common'
import { validate } from 'class-validator'
import { plainToClass } from 'class-transformer'

@Injectable()
// tslint:disable-next-line:no-any
export class ValidationPipe implements PipeTransform<any> {
  // tslint:disable-next-line:no-any
  async transform(value: any, { metatype }: ArgumentMetadata) {
    if (!metatype || !this.toValidate(metatype)) {
      return value
    }

    const object = plainToClass(metatype, value)

    const errors = await validate(object, { whitelist: true })

    if (errors && errors.length > 0) {
      throw new BadRequestException(errors)
    }

    return object
  }

  private toValidate(metatype: Function): boolean {
    const types: Function[] = [String, Boolean, Number, Array, Object]

    return !types.includes(metatype)
  }
}
