import { Module } from '@nestjs/common'
import { ProductsModule } from '../domains/products/products.module'
import { UsersModule } from '../domains/users/users.module'

export const swaggerImportModules = [
  UsersModule,
  ProductsModule,
]

@Module({
  imports: swaggerImportModules,
})
export class SwaggerModule { }
