import { Server } from 'http'
import { createServer, proxy } from 'aws-serverless-express'
import { eventContext } from 'aws-serverless-express/middleware'
import { NestFactory } from '@nestjs/core'
import { ExpressAdapter } from '@nestjs/platform-express'
import { Handler, Context } from 'aws-lambda'
import * as express from 'express'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'

async function bootstrapServer(module, swaggerModules): Promise<Server> {
  const expressApp = express()

  const adapter = new ExpressAdapter(expressApp)

  const nestApp = await NestFactory.create(
    module,
    adapter,
  )

  nestApp.enableCors()
  nestApp.use(eventContext())

  const options = new DocumentBuilder()
    .setTitle('Interview APIs')
    .setDescription(``)
    .setVersion('1.0')
    .build()

  const document = SwaggerModule.createDocument(
    nestApp,
    options,
    { include: swaggerModules },
  )

  SwaggerModule.setup(`swagger`, nestApp, document)

  await nestApp.init()

  const cachedServer = createServer(expressApp)

  return cachedServer
}

export const handler: Handler = (module, swaggerModules = []) => (event, context: Context) => {
  if (event.path === `/swagger`) {
    event.path = `/swagger/`
  }

  event.path = event.path.includes('swagger-ui')
    ? `/swagger${event.path}`
    : event.path

  return bootstrapServer(module, swaggerModules)
    .then(server => {
      const promise = proxy(
        server,
        event,
        context,
        'PROMISE',
      ).promise

      return promise
    })
}
