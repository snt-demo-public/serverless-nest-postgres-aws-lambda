import { Pool } from 'pg'

export const connectDb = (handler) => {
  return async function name() {
    let pool
    let client

    try {
      const environment = process.env

      if (!environment) {
        return
      }

      const databaseConfig = {
        host: environment.TYPEORM_HOST,
        user: environment.TYPEORM_USERNAME,
        password: environment.TYPEORM_PASSWORD,
        database: environment.TYPEORM_DATABASE,
        max: 20,
        idleTimeoutMillis: 30000,
        connectionTimeoutMillis: 20000,
      }

      if (!pool) {
        pool = new Pool(databaseConfig)
      }

      if (!client) {
        client = await pool.connect()
      }

      await handler(client)

      await client.end()
      await pool.end()
    } catch (error) {
      // tslint:disable-next-line:no-console
      console.log('------ HANDLER ERROR ------', error)

      if (client) {
        await client.end()
      }

      if (pool) {
        await pool.end()
      }
    }

    // tslint:disable-next-line:no-console
    console.log('-------------------DISCONNECT DATABASE-------------')
  }
}
