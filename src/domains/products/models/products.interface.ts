interface CreateProductData {
  userId: string
  name: string
}

interface UpdateProductData {
  name?: string
}

interface FindOneQuery {
  productId?: string
  userId?: string
  name?: string
}

// TODO: need to add more options to find many products
interface FindManyQuery {
  productId?: string
  userId?: string
  name?: string
}

export interface CreateProductService {
  data: CreateProductData
}

export interface UpdateProductService {
  query: FindOneQuery
  data: UpdateProductData
}

export interface FindOneProductService {
  query: FindOneQuery
}

export interface DeleteOneProductService {
  query: FindOneQuery
}

export interface FindManyProductsService {
  query: FindManyQuery
}
