import { User } from '../../users/models/users.schema'
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'

@Entity({ name: 'products' })
export class Product {
  @PrimaryGeneratedColumn('uuid')
  productId: string

  @ManyToOne(
    () => User,
    user => user.userId,
    { onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'userId' })
  @Column({ type: 'uuid' })
  userId: User | string

  @Column({ type: 'text' })
  name: string

  @CreateDateColumn({ type: 'timestamptz' })
  createdAt: Date

  @UpdateDateColumn({ type: 'timestamptz' })
  updatedAt: Date
}
