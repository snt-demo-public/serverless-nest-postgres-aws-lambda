import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { IsOptional, IsString } from 'class-validator'

export class CreateProductDto {
  @ApiProperty()
  @IsString()
  readonly name: string
}

export class UpdateProductDto {
  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  readonly name?: string
}

export class FindManyProductsDto {
  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  readonly productId?: string

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  readonly userId?: string

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  readonly name?: string
}
