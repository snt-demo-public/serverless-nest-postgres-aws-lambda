import { NotFoundException } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { EntityRepository, Repository } from 'typeorm'
import {
  CreateProductService,
  DeleteOneProductService,
  FindManyProductsService,
  FindOneProductService,
  UpdateProductService,
} from './models/products.interface'
import { Product } from './models/products.schema'

@EntityRepository(Product)
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private readonly productsRepository: Repository<Product>,
  ) { }

  async createOne({ data }: CreateProductService): Promise<Product> {
    try {
      const product = await this.productsRepository.save(data)

      return product
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async findOne({ query }: FindOneProductService): Promise<Product> {
    try {
      const product = await this.productsRepository.findOne({
        where: query,
        relations: ['userId'],
      })

      if (!product) {
        throw new NotFoundException('Product not found')
      }

      return product
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async findMany({ query }: FindManyProductsService): Promise<Product[]> {
    try {
      const products = await this.productsRepository.find({
        where: query,
        relations: ['userId'],
      })

      if (!products) {
        throw new NotFoundException('Product not found')
      }

      return products
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async updateOne({ query, data }: UpdateProductService): Promise<Product> {
    try {
      const product = await this.productsRepository.findOne({
        where: query,
      })

      if (!product) {
        throw new NotFoundException('Product not found')
      }

      await this.productsRepository.update(
        { productId: product.productId },
        data,
      )

      Object.assign(product, data)

      return product
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async deleteOne({ query }: DeleteOneProductService): Promise<boolean> {
    try {
      const product = await this.productsRepository.findOne({
        where: query,
      })

      if (!product) {
        throw new NotFoundException('Product not found')
      }

      await this.productsRepository.remove(product)

      return true
    } catch (error) {
      return Promise.reject(error)
    }
  }
}
