import { Body, Controller, Delete, Get, InternalServerErrorException, Param, Post, Put, Query, UsePipes } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'
import { ValidationPipe } from '../../middleware/pipes/validation.pipe'
import { CreateProductDto, UpdateProductDto } from './models/products.dto'
import { Product } from './models/products.schema'
import { ProductsService } from './products.service'

@ApiTags('Products')
@Controller()
export class ProductsController {
  constructor(
    private readonly productsService: ProductsService,
  ) { }

  @Post('users/:userId/products')
  @UsePipes(new ValidationPipe())
  async createOne(
    @Body() data: CreateProductDto,
    @Param() { userId },
  ): Promise<Product> {
    try {
      const product = await this.productsService.createOne({
        data: {
          ...data,
          userId,
        },
      })

      return product
    } catch (error) {
      throw new InternalServerErrorException(error)
    }
  }

  @Get('products/:productId')
  async findOne(
    @Param() { productId },
  ): Promise<Product> {
    try {
      const product = await this.productsService.findOne({
        query: { productId },
      })

      return product
    } catch (error) {
      throw new InternalServerErrorException(error)
    }
  }

  @Get('products')
  @UsePipes(new ValidationPipe())
  async findMany(
    @Query() query,
  ): Promise<Product[]> {
    try {
      const products = await this.productsService.findMany({ query })

      return products
    } catch (error) {
      throw new InternalServerErrorException(error)
    }
  }

  @Put('products/:productId')
  @UsePipes(new ValidationPipe())
  async updateOne(
    @Param() { productId },
    @Body() data: UpdateProductDto,
  ): Promise<Product> {
    try {
      const product = await this.productsService.updateOne({
        query: { productId },
        data,
      })

      return product
    } catch (error) {
      throw new InternalServerErrorException(error)
    }
  }

  @Delete('products/:productId')
  async deleteOne(
    @Param() { productId },
  ): Promise<boolean> {
    try {
      const isUpdated = await this.productsService.deleteOne({
        query: { productId },
      })

      return isUpdated
    } catch (error) {
      throw new InternalServerErrorException(error)
    }
  }
}
