import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { DatabaseModule } from '../../third_parties/database/database.module'
import { ConfigModule } from '../../configs/configs.module'
import { Product } from './models/products.schema'
import { ProductsController } from './products.controller'
import { ProductsService } from './products.service'

const ProductsRepository = TypeOrmModule.forFeature([Product])

@Module({
  imports: [
    DatabaseModule,
    ProductsRepository,
    ConfigModule,
  ],
  providers: [ProductsService],
  controllers: [ProductsController],
  exports: [ProductsService],
})
export class ProductsModule { }
