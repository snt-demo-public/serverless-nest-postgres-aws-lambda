import { connectDb } from '../../helpers/connect_db'

const handler = async (client) => {
  const productData = await client.query(`select * from products`)

  // tslint:disable-next-line:no-console
  console.log(productData.rows || 'No Product Found!')
}

export const productCronHandler = connectDb(handler)
