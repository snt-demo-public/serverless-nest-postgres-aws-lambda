import { Body, Controller, Get, InternalServerErrorException, Param, Post, Put, Query, UsePipes } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'
import { ValidationPipe } from '../../middleware/pipes/validation.pipe'
import { FindManyUsersDto, SignupDto, UpdateUserDto } from './models/users.dto'
import { User } from './models/users.schema'
import { UsersService } from './users.service'

@ApiTags('Users')
@Controller()
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
  ) { }

  @Post('auth/signup')
  @UsePipes(new ValidationPipe())
  async createOne(
    @Body() signupData: SignupDto,
  ): Promise<User> {
    try {
      const user = await this.usersService.createOne({
        data: signupData,
      })

      return user
    } catch (error) {
      throw new InternalServerErrorException(error)
    }
  }

  @Get('users/:userId')
  async findOne(
    @Param() { userId },
  ): Promise<User> {
    try {
      const user = await this.usersService.findOne({
        query: { userId },
      })

      return user
    } catch (error) {
      throw new InternalServerErrorException(error)
    }
  }

  @Get('users')
  @UsePipes(new ValidationPipe())
  async findMany(
    @Query() query: FindManyUsersDto,
  ): Promise<User[]> {
    try {
      const users = await this.usersService.findMany({ query })

      return users
    } catch (error) {
      throw new InternalServerErrorException(error)
    }
  }

  @Put('users/:userId')
  @UsePipes(new ValidationPipe())
  async updateOne(
    @Param() { userId },
    @Body() updateData: UpdateUserDto,
  ): Promise<User> {
    try {
      const user = await this.usersService.updateOne({
        query: { userId },
        data: updateData,
      })

      return user
    } catch (error) {
      throw new InternalServerErrorException(error)
    }
  }
}
