import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { DatabaseModule } from '../../third_parties/database/database.module'
import { ConfigModule } from '../../configs/configs.module'
import { User } from './models/users.schema'
import { UsersController } from './users.controller'
import { UsersService } from './users.service'

const UsersRepository = TypeOrmModule.forFeature([User])

@Module({
  imports: [
    DatabaseModule,
    UsersRepository,
    ConfigModule,
  ],
  providers: [UsersService],
  controllers: [UsersController],
  exports: [UsersService],
})
export class UsersModule { }
