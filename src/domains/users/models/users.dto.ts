import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { IsEmail, IsOptional, IsString, MaxLength } from 'class-validator'

export class SignupDto {
  @ApiProperty()
  @MaxLength(30)
  @IsString()
  readonly firstName: string

  @ApiProperty()
  @MaxLength(30)
  @IsString()
  readonly lastName: string

  @ApiProperty({ format: 'email' })
  @MaxLength(70)
  @IsEmail()
  readonly email: string

  @ApiProperty()
  @IsString()
  readonly password: string
}

export class UpdateUserDto {
  @ApiPropertyOptional()
  @IsString()
  @MaxLength(30)
  @IsOptional()
  readonly firstName?: string

  @ApiPropertyOptional()
  @IsString()
  @MaxLength(30)
  @IsOptional()
  readonly lastName?: string

  @ApiPropertyOptional({ format: 'email' })
  @IsEmail()
  @MaxLength(70)
  @IsOptional()
  readonly email?: string

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  readonly password?: string
}

export class FindManyUsersDto {
  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  userId?: string

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  firstName?: string

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  lastName?: string

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  email?: string
}
