import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'

@Entity({ name: 'users' })
export class User {
  @PrimaryGeneratedColumn('uuid')
  userId: string

  @Column({ type: 'varchar', length: '30', nullable: true })
  firstName: string

  @Column({ type: 'varchar', length: '30', nullable: true })
  lastName: string

  @Column({ type: 'varchar', length: '70', unique: true })
  email: string

  @Column({ type: 'text', select: false })
  password: string

  @CreateDateColumn({ type: 'timestamptz' })
  createdAt: Date

  @UpdateDateColumn({ type: 'timestamptz' })
  updatedAt: Date
}
