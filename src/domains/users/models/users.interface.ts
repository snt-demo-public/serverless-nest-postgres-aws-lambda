interface CreateUserData {
  firstName: string
  lastName: string
  email: string
  password: string
}

interface UpdateUserData {
  firstName?: string
  lastName?: string
  email?: string
  password?: string
}

interface FindOneQuery {
  userId?: string
  firstName?: string
  lastName?: string
  email?: string
}

// TODO: need to add more options to find many users
interface FindManyQuery {
  userId?: string
  firstName?: string
  lastName?: string
  email?: string
}

export interface CreateUserService {
  data: CreateUserData
}

export interface UpdateUserService {
  query: FindOneQuery
  data: UpdateUserData
}

export interface FindOneUserService {
  query: FindOneQuery
}

export interface FindManyUsersService {
  query: FindManyQuery
}
