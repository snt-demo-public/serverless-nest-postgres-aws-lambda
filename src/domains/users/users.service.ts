import { NotFoundException } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { EntityRepository, Repository } from 'typeorm'
import { CreateUserService, FindManyUsersService, FindOneUserService, UpdateUserService } from './models/users.interface'
import { User } from './models/users.schema'

@EntityRepository(User)
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
  ) { }

  async createOne({ data }: CreateUserService): Promise<User> {
    // TODO: need to hash user password
    const user = await this.usersRepository.save(data)

    return user
  }

  async findOne({ query }: FindOneUserService): Promise<User> {
    const user = await this.usersRepository.findOne({
      where: query,
    })

    if (!user) {
      throw new NotFoundException('User not found')
    }

    return user
  }

  async findMany({ query }: FindManyUsersService): Promise<User[]> {
    const users = await this.usersRepository.find({
      where: query,
    })

    return users
  }

  async updateOne({ data, query }: UpdateUserService): Promise<User> {
    const user = await this.usersRepository.findOne({
      where: query,
    })

    if (!user) {
      throw new NotFoundException('User not found')
    }

    await this.usersRepository.update(
      { userId: user.userId },
      data,
    )

    Object.assign(user, data)

    return user
  }
}
