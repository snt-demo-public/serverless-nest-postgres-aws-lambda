import { ProductsModule } from './domains/products/products.module'
import { UsersModule } from './domains/users/users.module'
import { handler } from './serverless/init_bootstrap_server'
import { SwaggerModule, swaggerImportModules } from './serverless/swagger.serverless'

export const swaggerHandler = handler(
  SwaggerModule,
  swaggerImportModules,
)

export const usersHandler = handler(UsersModule)

export const productsHandler = handler(ProductsModule)
