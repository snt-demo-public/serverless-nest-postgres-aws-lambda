import { seedingUserData } from '../constants/users'
import { seedingProductData } from '../constants/products'
import { getRepository, MigrationInterface, QueryRunner } from 'typeorm'

export class SeedingData1610772217633 implements MigrationInterface {

    public async up(_: QueryRunner): Promise<void> {
        const users = await getRepository('users').save(seedingUserData)

        console.log('users', users)

        const products = await getRepository('products').save(seedingProductData)

        console.log('products', products)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        //
    }

}
