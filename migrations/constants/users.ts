export const seedingUserData = [
  {
      userId: '6bd82e37-63de-45b8-817f-827b7cf9b0e4',
      firstName: 'User',
      lastName: '1',
      email: 'user1@gmail.com',
      password: '123123',
  },
  {
      userId: '0c14400a-f12b-41ab-b9ac-56e17c42ad88',
      firstName: 'User',
      lastName: '2',
      email: 'user2@gmail.com',
      password: '123123',
  },
  {
      userId: '567ef266-f3ab-42b6-816f-232d7fba1ab2',
      firstName: 'User',
      lastName: '3',
      email: 'user3@gmail.com',
      password: '123123',
  },
  {
      userId: 'a60049fa-6340-46e7-a308-24a44772e29f',
      firstName: 'User',
      lastName: '4',
      email: 'user4@gmail.com',
      password: '123123',
  },
  {
      userId: '4c199501-5eb0-42be-b18b-4580e2ef815c',
      firstName: 'User',
      lastName: '5',
      email: 'user5@gmail.com',
      password: '123123',
  }
]
