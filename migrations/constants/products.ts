import { generateString as generateUUID } from "@nestjs/typeorm";
import { seedingUserData } from "./users";

export const seedingProductData = [
  {
    productId: generateUUID(),
    name: 'Product A',
    userId: seedingUserData[0].userId,
  },
  {
    productId: generateUUID(),
    name: 'Product B',
    userId: seedingUserData[0].userId,
  },
  {
    productId: generateUUID(),
    name: 'Product C',
    userId: seedingUserData[1].userId,
  },
  {
    productId: generateUUID(),
    name: 'Product D',
    userId: seedingUserData[1].userId,
  },
  {
    productId: generateUUID(),
    name: 'Product E',
    userId: seedingUserData[2].userId,
  },
  {
    productId: generateUUID(),
    name: 'Product H',
    userId: seedingUserData[2].userId,
  },
  {
    productId: generateUUID(),
    name: 'Product F',
    userId: seedingUserData[3].userId,
  },
  {
    productId: generateUUID(),
    name: 'Product G',
    userId: seedingUserData[3].userId,
  },
  {
    productId: generateUUID(),
    name: 'Product I',
    userId: seedingUserData[4].userId,
  },
  {
    productId: generateUUID(),
    name: 'Product I',
    userId: seedingUserData[4].userId,
  },
]