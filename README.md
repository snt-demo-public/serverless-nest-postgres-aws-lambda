## Features

- Serverless framework
- NestJS framework
- Scheduled functions
- APIs endpoints
- Typescript
- PostgresQL 
- TypeORM
- schema migration & data seeding scripts 
- auto generated API docs (swagger) with open API standards
- AWS lambda + AWS api gateway Deployment
- Business domain driven design 
- Input/DTO validation
- Controllers/services/repositories
- Tslint for linting
- Environment configurations (dev/local)

## Prerequisites

- Node.js: ^12.0.0
- npm: ^6.0.0
- Postgres: 12.4
- macOS or linux recommended 

## Initial project

```bash

  $npm install

  $npm install -g serverless

```

## Run project in local

```bash
  $npm run start:local

```

## Deployment

```bash
  $npm run deploy:dev

```

## Debugging

- step 1: In the .vscode folder, create launch.json file
- step 2: Adding the below code to launch.json file

```bash

{
  "version": "0.2.0",
  "configurations": [
    {
      "name": "Serverless Offline",
      "type": "node",
      "request": "launch",
      "cwd": "${workspaceFolder}",
      "program": "${workspaceRoot}/node_modules/serverless/bin/serverless",
      "args": [
        "offline",
        "--noAuth",
        "--stage=local",
      ],
      "protocol": "inspector",
      "runtimeExecutable": "node",
      "port": 3000,
    }
  ]
}

```

- step 3: Running debug - Run -> Start Debugging

## Endpoints

- Example: POST http://localhost:3000/local/auth/signup
- Example: GET http://localhost:3000/local/users
- Example: GET http://localhost:3000/local/users/{userId}
- Example: PUT http://localhost:3000/local/users/{userId}

- Example: POST http://localhost:3000/local/users/{userId}/products
- Example: GET http://localhost:3000/local/products
- Example: GET http://localhost:3000/local/products/{productId}
- Example: PUT http://localhost:3000/local/products/{productId}
- Example: DELETE local/products/{productId}

## Swagger - APIs docs

- Local: http://localhost:3000/local/swagger
